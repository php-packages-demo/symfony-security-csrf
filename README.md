# symfony/[security-csrf](https://packagist.org/packages/symfony/security-csrf)

![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/security-csrf)
CSRF Library

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-http-foundation+php-symfony-mime+php-symfony-http-kernel+php-symfony-error-handler+php-symfony-property-access+php-symfony-css-selector+php-symfony-property-info+php-symfony-intl+php-symfony-options-resolver&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-options-resolver+php-symfony-dom-crawler+php-symfony-browser-kit+php-symfony-ldap+php-symfony-lock+php-symfony-asset+php-symfony-form+php-symfony-security-core+php-symfony-security-csrf&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

# Official documentation
* https://symfony.com/doc/current/components/security

# Unofficial documentation
* [*Web Application Security: An Introduction to CSRF Tokens in Symfony*
  ](https://medium.com/@skowron.dev/web-application-security-an-introduction-to-csrf-tokens-in-symfony-f6da6b706fc0)
  2024-01 Jakub Skowron (skowron.dev) @ Medium
* [*Combatting Login CSRF with Symfony*
  ](https://wouterj.nl/2023/10/combatting-login-csrf-with-symfony)
  2023-10 Wouter de Jong
* (fr) [*Comprendre et éviter les attaques CSRF grâce à Symfony*
  ](https://jolicode.com/blog/comprendre-et-eviter-les-attaques-csrf-grace-a-symfony)
  2023-03 Jérôme Gangneux
* (fr) [*Protéger son application Symfony contre les attaques CSRF*
  ](https://blog.netinfluence.ch/2019/05/03/proteger-son-application-symfony-contre-les-attaques-csrf/)
  2019-05 [Romaric](https://blog.netinfluence.ch/author/romaric/)
